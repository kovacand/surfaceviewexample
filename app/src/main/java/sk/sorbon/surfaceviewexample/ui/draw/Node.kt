package sk.sorbon.surfaceviewexample.ui.draw

class Node(var x: Float, var y: Float, var title: String) {

    var savedX = 0f
    var savedY = 0f

    fun save() {
        savedX = x
        savedY = y
    }


}