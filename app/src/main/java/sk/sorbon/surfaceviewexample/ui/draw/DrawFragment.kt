package sk.sorbon.surfaceviewexample.ui.draw

import android.app.Dialog
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.Paint
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.text.SpannableStringBuilder
import android.view.*
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import androidx.core.graphics.withTranslation
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.draw_fragment.*
import sk.sorbon.surfaceviewexample.R
import kotlin.math.abs
import kotlin.math.sqrt

class DrawFragment : Fragment() {

    companion object {
        fun newInstance() = DrawFragment()

        fun dist(x: Float, y: Float) = sqrt(x * x + y * y)

        private const val NODE_WIDTH = 200f
        private const val NODE_HEIGHT = 100f
    }

    private val blockPaint = Paint()
    private val textPaint = Paint()
    private var touchedNode: Node? = null

    private var translateX = 0f
    private var translateY = 0f

    init {
        blockPaint.color = Color.BLACK
        blockPaint.style = Paint.Style.STROKE
        blockPaint.strokeWidth = 2f
        textPaint.color = Color.BLACK
        textPaint.style = Paint.Style.FILL_AND_STROKE
        textPaint.textSize = 60f
    }

    private lateinit var viewModel: DrawViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        return inflater.inflate(R.layout.draw_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        sv_canvas.holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS)
        sv_canvas.holder.addCallback(object : SurfaceHolder.Callback {
            override fun surfaceChanged(holder: SurfaceHolder?, fmt: Int, w: Int, h: Int) {
                translateX = w / 2f
                translateY = h / 2f
                redraw()
            }
            override fun surfaceDestroyed(p0: SurfaceHolder?) {}
            override fun surfaceCreated(holder: SurfaceHolder?) {}
        })
        sv_canvas.setOnTouchListener(object : View.OnTouchListener {
            var downX = 0f
            var downY = 0f
            override fun onTouch(view: View?, event: MotionEvent?): Boolean {
                return when (event?.action) {
                    MotionEvent.ACTION_DOWN -> {
                        downX = event.x
                        downY = event.y
                        val x = event.x - translateX
                        val y = event.y - translateY
                        viewModel.nodes.forEach {
                            if (abs(x - it.x) < NODE_WIDTH / 2 && abs(y - it.y) < NODE_HEIGHT / 2f) {
                                touchedNode = it
                                println("touched")
                            }
                        }
                        touchedNode?.save()
                        true
                    }
                    MotionEvent.ACTION_MOVE -> {
                        val node = touchedNode ?: return true
                        node.x = node.savedX + (event.x - downX)
                        node.y = node.savedY + (event.y - downY)
                        redraw()
                        true
                    }
                    MotionEvent.ACTION_UP -> {
                        val node = touchedNode ?: return true
                        if (dist(downX - event.x, downY - event.y) < 10f) onClick(node)
                        touchedNode = null
                        true
                    }
                    else -> false
                }
            }
        })
        b_add_new.setOnClickListener {
            viewModel.nodes.add(Node(0f, 0f, "Hello"))
            redraw()
        }
    }

    private fun onClick(node: Node) {
        val et = EditText(activity)
        et.text = SpannableStringBuilder(node.title)
        AlertDialog.Builder(context!!)
            .setView(et)
            .setTitle("Edit")
            .setPositiveButton("OK") { _, _ ->
                node.title = et.text.toString()
                redraw()
            }.show()
    }

    private fun redraw() {
        val radius = 30f
        val c = sv_canvas.holder.lockCanvas() ?: return
        c.drawRGB(200, 200, 200)
        c.withTranslation(translateX, translateY) {
            viewModel.nodes.forEach {
                this.withTranslation(it.x, it.y) {
                    this.drawRect(
                        -NODE_WIDTH / 2f,
                        -NODE_HEIGHT / 2f,
                        NODE_WIDTH / 2f,
                        NODE_HEIGHT / 2f,
                        blockPaint
                    )
                    this.drawCircle(-NODE_WIDTH / 2f - radius, 0f, radius, blockPaint)
                    this.drawCircle(NODE_WIDTH / 2f + radius, 0f, radius, blockPaint)
                    this.drawText(
                        it.title,
                        -NODE_WIDTH / 2f + 10f,
                        textPaint.textSize / 2f,
                        textPaint
                    )
                }
            }
        }
        sv_canvas.holder.unlockCanvasAndPost(c)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(DrawViewModel::class.java)

    }

}
