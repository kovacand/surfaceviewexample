package sk.sorbon.surfaceviewexample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import sk.sorbon.surfaceviewexample.ui.draw.DrawFragment

class DrawActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.draw_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container, DrawFragment.newInstance())
                .commitNow()
        }
    }

}
